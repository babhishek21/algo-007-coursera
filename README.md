# algo-007-Coursera
Implementations of common algorithms enunciated in [Coursera](https://www.coursera.org/) [algo-007](https://www.coursera.org/course/algo) course lectures.
The lectures are taken by Prof. Tim Roughgarden of Stanford University, USA.

## Folder Structure
- **Algo**: Contains common algorithm implementations, which were **NOT** asked directly for *creditable* work.
- **Test cases**: Contains my test-cases. These **do not** include original P-Set test cases.
	- **Subfolders** of form `P*` where `*` would reperesent the week number/ P-Set number.

## License and Contributing
Shared under [MIT License](http://opensource.org/licenses/MIT). Read the [LICENSE](/LICENSE) file.

Feel free to contribute (*as long as you go by the license*).
